# Recap

## Ansible

- Engineer with a set of work items to acomplish
  - Password policy
  - Install and configure software like a webserver/database server software

- Ansible needs to know how to do the task
  - Modules
    - package
    - lineinfile
    - templates

- The module needs to know what it should do
  - Tasks
    - Uses variables to interact with modules

- Grouping of tasks/templates/files to perform a set of tasks and fullfill a specific role
  - Roles
    - Should only serve one specific purpose
    - Should be as small as possible

- Ansible needs to know where those tasks should be performed
  - Inventories

- Inventories consist of hosts and host groups
  - Host is a computer/network hardware/..
  - Hosts can be grouped together
    - often for their purpose
      - mailservers/webservers/database servers/...
    - needed information can be stored on a per host or per group basis with host_vars and group_vars

- Ansible needs a concise way of getting this combined information
  - Playbooks

- Humans need to create these playbooks so they need a human readable format
  - YAML Files

## Automation

- Increased Efficiency
  - tasks completed faster and more consistently
  - eliminating manual intervention - reduces human errors

- Time and Cost Savings
  - repetitive and mundane tasks are automated

- Improved Accuracy and Reliability
  - minimizes human error
  - improved accuracy and reliability
  - consistently produce the same outcome

- Scalability and Flexibility
  - easy scalable and adaptabe to changing demands
  - flexibility for easily adjusted processes

- Enhanced Consistency and Compliance
  - ensure predefined standards and compliance requirements
  - enforcing standardized workflows
  - reduces risk of deviations and non-compliance


## See also

- <https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html>
- <https://docs.ansible.com/ansible/latest/reference_appendices/glossary.html>
